package com.carbonit.katas.jarouna.bank.formatters;

import com.carbonit.katas.jarouna.bank.entities.Operation;

import java.time.ZoneId;
import java.util.List;

public interface StatementFormatter {
    String format(List<Operation> operations, ZoneId zoneId);
}
