package com.carbonit.katas.jarouna.bank.repositories;

import com.carbonit.katas.jarouna.bank.entities.Operation;

import java.util.List;
import java.util.Optional;

public interface OperationRepository {
    void add(Long accountId, Operation operation);

    List<Operation> findAll(Long accountId);

    Optional<Operation> findLast(Long accountId);
}
