package com.carbonit.katas.jarouna.bank.entities;

public enum OperationType {
    DEPOSIT, WITHDRAWAL
}
