package com.carbonit.katas.jarouna.bank.outputs;

public interface OutputInterface {
    void output(String message);
}
