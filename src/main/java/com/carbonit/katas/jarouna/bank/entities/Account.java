package com.carbonit.katas.jarouna.bank.entities;

import com.carbonit.katas.jarouna.bank.formatters.StatementFormatter;
import com.carbonit.katas.jarouna.bank.outputs.OutputInterface;
import com.carbonit.katas.jarouna.bank.repositories.OperationRepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

import static java.time.ZoneOffset.UTC;

public class Account {

    private static final String AMOUNT_NEGATIVE_ERR_MESSAGE = "The amount of the operation can't be negative";
    private static final String NOT_ENOUGH_PROVISION_ERR_MESSAGE = "This account doesn't have enough provision for a withdrawal";
    private static final BigDecimal OVERDRAFT_LIMIT = BigDecimal.ZERO;

    private final Long id;
    private final OperationRepository operationRepository;
    private final OutputInterface outputInterface;

    public Account(Long id, OperationRepository operationRepository, OutputInterface outputInterface) {
        this.id = id;
        this.operationRepository = operationRepository;
        this.outputInterface = outputInterface;
    }

    public void deposit(BigDecimal amount, ZonedDateTime zonedDateTime) {
        throwExceptionIfAmountNegative(amount);

        BigDecimal newBalance = getLastBalance().add(amount);

        LocalDateTime utcDateTime = zonedDateTime.withZoneSameInstant(UTC).toLocalDateTime();

        operationRepository.add(id, new Operation(amount, utcDateTime, OperationType.DEPOSIT, newBalance));
    }

    public void withdraw(BigDecimal amount, ZonedDateTime zonedDateTime) {
        throwExceptionIfAmountNegative(amount);
        BigDecimal lastBalance = getLastBalance();
        throwExceptionIfAccountHasNoProvisionLeft(lastBalance, amount);
        BigDecimal newBalance = lastBalance.subtract(amount);

        LocalDateTime utcDateTime = zonedDateTime.withZoneSameInstant(UTC).toLocalDateTime();

        operationRepository.add(id, new Operation(amount, utcDateTime, OperationType.WITHDRAWAL, newBalance));
    }

    private BigDecimal getLastBalance() {
        Optional<Operation> lastOperation = operationRepository.findLast(id);
        return lastOperation.isPresent() ? lastOperation.get().getBalance() : BigDecimal.ZERO;
    }

    private void throwExceptionIfAmountNegative(BigDecimal amount) throws IllegalArgumentException {
        if (amount.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException(AMOUNT_NEGATIVE_ERR_MESSAGE);
        }
    }

    private void throwExceptionIfAccountHasNoProvisionLeft(BigDecimal balance, BigDecimal amount) throws IllegalArgumentException {
        if (balance.subtract(amount).compareTo(OVERDRAFT_LIMIT) < 0) {
            throw new IllegalStateException(NOT_ENOUGH_PROVISION_ERR_MESSAGE);
        }
    }

    public void outputStatement(StatementFormatter statementFormatter, ZoneId zoneId) {
        outputInterface.output(statementFormatter.format(operationRepository.findAll(id), zoneId));
    }
}
