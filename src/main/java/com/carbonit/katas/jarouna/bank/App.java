package com.carbonit.katas.jarouna.bank;

import com.carbonit.katas.jarouna.bank.entities.Account;
import com.carbonit.katas.jarouna.bank.entities.AccountBuilder;
import com.carbonit.katas.jarouna.bank.formatters.GridStatementFormatter;
import com.carbonit.katas.jarouna.bank.formatters.StatementFormatter;
import com.carbonit.katas.jarouna.bank.outputs.ConsoleOutputInterface;
import com.carbonit.katas.jarouna.bank.outputs.OutputInterface;
import com.carbonit.katas.jarouna.bank.repositories.InMemoryOperationRepository;
import com.carbonit.katas.jarouna.bank.repositories.OperationRepository;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static java.time.ZoneOffset.UTC;

public class App
{

    private static final ZoneId SYSTEM_DEFAULT_ZONE_ID = ZoneId.systemDefault();
    private static final String EMPTY = "";

    public static void main( String[] args )
    {

        StatementFormatter statementFormatter = new GridStatementFormatter();
        OperationRepository operationRepository = new InMemoryOperationRepository();
        OutputInterface outputInterface = new ConsoleOutputInterface();

        ZonedDateTime creationDateTime = ZonedDateTime.of(2020, 6, 8, 0, 0, 0, 0, UTC);
        Account account = new AccountBuilder()
                .build(1L, operationRepository, outputInterface, creationDateTime);

        account.deposit(BigDecimal.valueOf(1000), creationDateTime);
        account.deposit(BigDecimal.valueOf(2000), creationDateTime.plusDays(1));
        account.withdraw(BigDecimal.valueOf(500), creationDateTime.plusDays(3));

        account.outputStatement(statementFormatter, SYSTEM_DEFAULT_ZONE_ID);

        System.out.println(EMPTY);

        ZonedDateTime anotherCreationDateTime = ZonedDateTime.of(2020, 6, 2, 0, 0, 0, 0, UTC);
        Account anotherAccount = new AccountBuilder()
                .withInitialAmount(BigDecimal.valueOf(5000))
                .build(2L, operationRepository, outputInterface, anotherCreationDateTime);

        anotherAccount.withdraw(BigDecimal.valueOf(2500), anotherCreationDateTime.plusDays(4));
        anotherAccount.withdraw(BigDecimal.valueOf(1500), anotherCreationDateTime.plusDays(6));
        anotherAccount.deposit(BigDecimal.valueOf(5000), anotherCreationDateTime.plusDays(8));

        anotherAccount.outputStatement(statementFormatter, SYSTEM_DEFAULT_ZONE_ID);
    }

}
