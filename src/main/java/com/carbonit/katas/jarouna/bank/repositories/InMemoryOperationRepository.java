package com.carbonit.katas.jarouna.bank.repositories;


import com.carbonit.katas.jarouna.bank.entities.Operation;

import java.util.*;

public class InMemoryOperationRepository implements OperationRepository {
    private final Map<Long, List<Operation>> operations = new HashMap<>();

    @Override
    public void add(Long accountId, Operation operation) {
        if(!operations.containsKey(accountId)) operations.put(accountId, new ArrayList<>());
        operations.get(accountId).add(operation);
    }

    @Override
    public List<Operation> findAll(Long accountId) {
        List<Operation> accountOperations = findAllByAccountId(accountId);
        return Collections.unmodifiableList(accountOperations);
    }

    @Override
    public Optional<Operation> findLast(Long accountId) {
        List<Operation> accountOperations = findAllByAccountId(accountId);
        return !accountOperations.isEmpty() ?
                Optional.of(accountOperations.get(accountOperations.size() - 1)) : Optional.empty();
    }

    private List<Operation> findAllByAccountId(Long accountId) {
        return operations.containsKey(accountId) ? operations.get(accountId) : new ArrayList<>();
    }
}
