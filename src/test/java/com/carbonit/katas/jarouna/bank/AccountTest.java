package com.carbonit.katas.jarouna.bank;

import com.carbonit.katas.jarouna.bank.entities.Account;
import com.carbonit.katas.jarouna.bank.entities.Operation;
import com.carbonit.katas.jarouna.bank.entities.OperationType;
import com.carbonit.katas.jarouna.bank.formatters.StatementFormatter;
import com.carbonit.katas.jarouna.bank.outputs.OutputInterface;
import com.carbonit.katas.jarouna.bank.repositories.OperationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InOrder;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class AccountTest {

    private static final ZoneId PARIS_ZONE_ID = ZoneId.of("Europe/Paris");
    private static final ZonedDateTime BASE_ZONED_DATE_TIME = LocalDateTime.of(2020, 6, 8, 12, 14)
            .atZone(PARIS_ZONE_ID);
    private static final LocalDateTime BASE_DATE_TIME_UTC = LocalDateTime.of(2020, 6, 8, 10, 14);

    private OperationRepository operationRepository;
    private StatementFormatter statementFormatter;
    private OutputInterface outputInterface;

    private final Long accountId = 1L;
    private Account account;

    private final List<Operation> EMPTY_OPERATION_LIST = Collections.emptyList();

    @BeforeEach
    void set_up(){
        operationRepository = mock(OperationRepository.class);
        statementFormatter = mock(StatementFormatter.class);
        outputInterface = mock(OutputInterface.class);
        account = new Account(accountId, operationRepository, outputInterface);
    }

    @DisplayName("As a bank client I can make a deposit in my account")
    @Test
    void given_a_valid_amount_expect_the_deposit_operation_to_be_added_to_the_repository() {
        BigDecimal initAmount = BigDecimal.valueOf(1000);
        BigDecimal amount = BigDecimal.valueOf(100);
        BigDecimal balance = BigDecimal.valueOf(1100);

        Operation lastOperation = new Operation(initAmount, BASE_DATE_TIME_UTC, OperationType.DEPOSIT, initAmount);
        Operation operation = new Operation(amount, BASE_DATE_TIME_UTC, OperationType.DEPOSIT, balance);

        given(operationRepository.findLast(accountId)).willReturn(Optional.of(lastOperation));

        account.deposit(amount, BASE_ZONED_DATE_TIME);

        InOrder order = Mockito.inOrder(operationRepository);

        order.verify(operationRepository).findLast(accountId);
        order.verify(operationRepository).add(accountId, operation);
        order.verifyNoMoreInteractions();
    }

    @DisplayName("As a bank client I can't make a deposit in my account with a negative amount")
    @Test
    void given_a_negative_amount_for_a_deposit_expect_an_IllegalArgumentException() {
        Executable executable = () -> {
            BigDecimal amount = BigDecimal.valueOf(-100);

            account.deposit(amount, BASE_ZONED_DATE_TIME);
        };

        assertThrows(IllegalArgumentException.class, executable);
    }

    @DisplayName("As a bank client I can make a withdrawal from my account")
    @Test
    void given_a_valid_amount_expect_the_withdrawal_operation_to_be_added_to_the_repository() {
        BigDecimal initAmount = BigDecimal.valueOf(1000);
        BigDecimal amount = BigDecimal.valueOf(300);
        BigDecimal balance = BigDecimal.valueOf(700);

        Operation lastOperation = new Operation(initAmount, BASE_DATE_TIME_UTC, OperationType.DEPOSIT, initAmount);
        Operation operation = new Operation(amount, BASE_DATE_TIME_UTC,
                OperationType.WITHDRAWAL, balance);

        given(operationRepository.findLast(accountId)).willReturn(Optional.of(lastOperation));

        account.withdraw(amount, BASE_ZONED_DATE_TIME);

        InOrder order = Mockito.inOrder(operationRepository);

        order.verify(operationRepository).findLast(accountId);
        order.verify(operationRepository).add(accountId, operation);
        order.verifyNoMoreInteractions();
    }

    @DisplayName("As a bank client I can't make a withdrawal from my account if it doesn't have enough provision")
    @Test
    void given_not_enough_provision_for_a_withdrawal_expect_an_IllegalStateException() {
        Executable executable = () -> {
            BigDecimal amount = BigDecimal.valueOf(2000);

            account.withdraw(amount, BASE_ZONED_DATE_TIME);
        };

        assertThrows(IllegalStateException.class, executable);
    }

    @DisplayName("As a bank client I can't make a withdrawal from my account with a negative amount")
    @Test
    void given_a_negative_amount_for_a_withdrawal_expect_an_IllegalArgumentException() {
        Executable executable = () -> {
            BigDecimal amount = BigDecimal.valueOf(-100);

            account.withdraw(amount, BASE_ZONED_DATE_TIME);
        };

        assertThrows(IllegalArgumentException.class, executable);
    }

    @DisplayName("As a bank client I can see the statement of my account")
    @Test
    void given_an_account_expect_the_statement() {
        List<Operation> emptyOperationList = EMPTY_OPERATION_LIST;
        String expectedFormattedStatements = "";

        given(operationRepository.findAll(accountId)).willReturn(emptyOperationList);
        given(statementFormatter.format(EMPTY_OPERATION_LIST, PARIS_ZONE_ID))
                .willReturn(expectedFormattedStatements);

        InOrder order = Mockito.inOrder(operationRepository, statementFormatter, outputInterface);

        account.outputStatement(statementFormatter, PARIS_ZONE_ID);

        order.verify(operationRepository).findAll(accountId);
        order.verify(statementFormatter).format(emptyOperationList, PARIS_ZONE_ID);
        order.verify(outputInterface).output(expectedFormattedStatements);

        verifyNoMoreInteractions(operationRepository);
        verifyNoMoreInteractions(statementFormatter);
        verifyNoMoreInteractions(outputInterface);
    }
}
